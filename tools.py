
import h5py
import numpy as np
import matplotlib.pyplot as plt
import ROOT as root
def calculate_distance(eta1, phi1, eta2, phi2):
    """Calculate the distance between two points in the η-φ plane."""
    dphi = np.abs(phi1 - phi2)
    dphi = np.where(dphi > np.pi, 2 * np.pi - dphi, dphi)
    return np.sqrt((eta1 - eta2) ** 2 + dphi ** 2)

def get_cutvalue(tagger="GN2v01", CDI_File='/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2023-02_MC20_CDI_GN2v01-noSF.root', jet_collection="AntiKt4EMPFlowJets", WP=65):
    """Retrieve cut values and fractions from the CDI file."""
    CDI_File = root.TFile.Open(CDI_File)
    if not CDI_File or CDI_File.IsZombie():
        raise RuntimeError("Failed to open CDI file.")
    
    cut_value_arr = []
    TVectorT_float = root.TVectorT('float')

    for wp in [65, 70, 77, 85, 90]:
        # Navigate to the specified directory
        dir_path = f"/{tagger}/{jet_collection}/FixedCutBEff_{wp}/"
        dir = CDI_File.GetDirectory(dir_path)
        if not dir:
            raise RuntimeError(f"Failed to navigate to directory: {dir_path}")
        
        print("Getting cut value from:", dir_path)
        
        # Retrieve TVector cutvalue object from WP directory
        cut_value = dir.Get("cutvalue")
        if isinstance(cut_value, (root.TVectorD, TVectorT_float)):
            cut_value_arr.append(cut_value[0])
        else:
            raise ValueError(f"Unknown type: {type(cut_value)}")
       
    fc = dir.Get("fraction")
    ftau = dir.Get("fraction_tau")

    # Access values correctly assuming they are TVector objects
    if isinstance(fc, (root.TVectorD, TVectorT_float)):
        fc_val = fc[0] 
    else:
        raise ValueError(f"Unknown type: {type(fc)}")
    if isinstance(ftau, (root.TVectorD, TVectorT_float)):
        ftau_val = ftau[0]
    else:
        raise ValueError(f"Unknown type: {type(ftau)}")
    
    return cut_value_arr, fc_val, ftau_val

# function to obtain PDG truth label
def _get_truth_label(flavour):
    if flavour.lower() == ("b" or "bottom"):
        return 5
    elif flavour.lower() == ("c" or "charm"):
        return 4
    elif flavour.lower() == ("l" or "light"):
        return 0
    else:
        print("Type not found, available flavours: b,c,l")

def load_jet_data(h5_file):
    """Load jet data from the HDF5 file."""
    with h5py.File(h5_file, 'r') as f:
        jets = f['jets']
        data = {
            'event_numbers': jets['eventNumber'][:],
            'eta': jets['eta'][:],
            'phi': jets['phi'][:],
            'pt': jets['pt'][:],
            'pb': jets['GN2v01_pb'][:],
            'pc': jets['GN2v01_pc'][:],
            'pu': jets['GN2v01_pu'][:],
            'ptau': jets["GN2v01_ptau"][:],
            'pdg_id': jets['HadronConeExclTruthLabelID'][:]
        }
    return data

def calculate_discriminant(data, cutvalues):
    """Calculate the discriminator value."""
    fc = cutvalues[1]
    ftau = cutvalues[2]
    return np.log(data['pb'] / (data['pu'] * (1 - fc - ftau ) + data['ptau'] * ftau + data['pc'] * fc))

