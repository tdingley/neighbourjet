import h5py
import numpy as np
import matplotlib.pyplot as plt
import ROOT as root
from tools import get_cutvalue, calculate_distance, load_jet_data, calculate_discriminant, _get_truth_label
import os


def process_events(data, cutvalues):
    events = {}
    event_indices = {}
    
    # Collect data
    for i in range(len(data['event_numbers'])):
        event = data['event_numbers'][i]
        if event not in events:
            events[event] = {'jets': [], 'leading_jet_pt': 0, 'leading_pdg_id': None, 'min_distance': np.inf}
            event_indices[event] = []

        event_indices[event].append(i)
        discrim = calculate_discriminant(data, cutvalues)
        pt = data['pt'][0]
        pdg_id = data['pdg_id'][0]
        events[event]['jets'].append((data['eta'][i], data['phi'][i], pt, pdg_id, discrim))
        
        #if pt > events[event]['leading_jet_pt']:
        events[event]['leading_jet_pt'] = pt
        events[event]['leading_pdg_id'] = pdg_id
    # now computing min_distance
    print("Now computing minimum distance for each event")
    # Compute min_distance
    for event, indices in event_indices.items():
        jets = events[event]['jets']
        n = len(jets)
        if n < 2:
            continue  # Skip events with fewer than 2 jets

        min_dist = np.inf
        for i in range(n):
            eta1, phi1, _, _, _ = jets[i]
            for j in range(i + 1, n):
                eta2, phi2, _, _, _ = jets[j]
                dist = calculate_distance(eta1, phi1, eta2, phi2)
                if dist < min_dist:
                    min_dist = dist
        events[event]['min_distance'] = min_dist

    return events

def plot_results(leading_jet_pt_above, leading_jet_pt_below, min_distance_threshold):
    """Plot results and save to files."""
    plt.figure(figsize=(10, 6))
    plt.hist(leading_jet_pt_above, bins=50, alpha=0.75, color='b', edgecolor='black', label='Min Distance > 2')
    plt.hist(leading_jet_pt_below, bins=50, alpha=0.75, color='r', edgecolor='black', label='Min Distance <= 2')
    plt.title('Distribution of Leading Jet Transverse Momentum')
    plt.xlabel('Leading Jet $p_T$ [GeV]')
    plt.ylabel('Frequency')
    plt.legend()
    plt.grid(True)
    plt.savefig('leading_jet_pt_plot.pdf')
    #plt.show()

def plot_2d_histogram(leading_jet_pt, min_distances, min_distance_threshold):
    """Plot a 2D histogram of leading jet pt vs min_distance for each event."""
    plt.figure(figsize=(10, 6))
    plt.hist2d(leading_jet_pt, min_distances, bins=50, cmap='viridis')
    plt.colorbar(label='Frequency')
    plt.xlabel('Leading Jet $p_T$ [GeV]')
    plt.ylabel('Minimum Distance')
    plt.title(f'2D Histogram of Leading Jet $p_T$ vs Min Distance (Threshold={min_distance_threshold})')
    plt.grid(True)
    plt.savefig('2d_histogram.png')
    plt.show()

def plot_raw_data(data):
    """
    data = {
        'event_numbers': jets['eventNumber'][:],
        'eta': jets['eta'][:],
        'phi': jets['phi'][:],
        'pt': jets['pt'][:],
        'pb': jets['GN2v01_pb'][:],
        'pc': jets['GN2v01_pc'][:],
        'pu': jets['GN2v01_pu'][:],
        'ptau': jets["GN2v01_ptau"][:],
        'pdg_id': jets['HadronConeExclTruthLabelID'][:]
    }
    """
    for variable in data.keys():
        print(f"Plotting {variable} in dataset")
        fig, ax = plt.subplots()
        ax.hist(data[variable],bins=30, density=True, alpha=0.6, color='skyblue', edgecolor='black')
        # Create the histogram

        ax.set_xlabel(variable, fontsize=14)
        ax.set_ylabel('Jets', fontsize=14)
        ax.grid(True, linestyle='--', alpha=0.7)

        fig.savefig(f"../Figures/{variable}.pdf")

def plot_efficiencies(data, cutvalues, flavour, variation = 'Nominal'):
    # Filter data to include only jets with pdg_id == 5 for b-jets, 0: light, 1: charm
    if not(os.path.exists(f"../Figures/{variation}")):
        os.mkdir(f"../Figures/{variation}")
        os.mkdir(f"../Figures/{variation}/{flavour}")
    # Calculate the discriminator values
    discrim = np.array(calculate_discriminant(data, cutvalues))
    WPs = [65, 70, 77, 85, 90]
    root_file = root.TFile(f"../Figures/{variation}/{flavour}/tagged.root", "RECREATE")

    for wp, cutvalue in enumerate(cutvalues[0]):
        # Apply cut and get tagged jets
        tagged = discrim > cutvalue
        jets_untagged = data['pt']
        jets_tagged = jets_untagged[tagged]

        # Define the bins for the histogram
        bins = np.linspace(0, 3e6, 20)

        # Calculate the histogram for the tagged data
        hist_tagged, _ = np.histogram(jets_tagged, bins=bins)

        # Calculate the histogram for all data
        hist_all, _ = np.histogram(jets_untagged, bins=bins)

        # Divide the binned values of the tagged data by the corresponding bins of all data
        hist_ratio = hist_tagged / hist_all

        # Create the figure and axes
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 6), sharex=True)

        # Plot the histogram of all data on the first axis
        ax1.hist(jets_untagged, bins=bins, color='b', alpha=0.5, label='All data')
        ax1.set_ylabel('Frequency', color='b')
        ax1.legend()

        # Plot the ratio on the second axis
        ax2.plot(bins[:-1], hist_ratio, color='r', marker='o', label='Ratio of tagged to all data')
        ax2.set_ylabel('Ratio', color='r')
        ax2.set_xlabel('Data')
        ax2.legend()

        plt.tight_layout()

        print(f"Plotting {WPs[wp]} pt spectrum")
        fig.savefig(f"../Figures/{variation}/{flavour}/tagged_{WPs[wp]}.pdf")
        hist_tagged_root = root.TH1F(f"tagged_{WPs[wp]}", "Tagged Jets Pt", len(bins) - 1, bins)
        hist_all_root = root.TH1F(f"truth_{WPs[wp]}", "All Jets Pt", len(bins) - 1, bins)

        for i in range(len(hist_tagged)):
            hist_tagged_root.SetBinContent(i+1, hist_tagged[i])
            hist_all_root.SetBinContent(i+1, hist_all[i])

        hist_tagged_root.Write()
    hist_all_root.Write()
    root_file.Close()
    
def efficiency_variation(events):
    # now we do this per-event
    fig, ax = plt.subplots()
    ax.hist(events[:]['leading_jet_pt'])
    fig.savefig("Test_leadingpt.pdf")



def main(h5_file, min_distance_threshold, flavour):
    data = load_jet_data(h5_file)
    pdg = _get_truth_label(flavour)
    data = {key: value[data['pdg_id'] == pdg] for key, value in data.items()}


    plot_raw_data(data)
    cutvalues = get_cutvalue()
    plot_efficiencies(data, cutvalues, 'b')

    #events = process_events(data, cutvalues)
    #efficiency_variation(events)
    #plot_2d_histogram(leading_jet_pt_above + leading_jet_pt_below, min_distances, min_distance_threshold)
    #plot_2d_histogram(events[:]['leading_jet_pt'], events[:]['min_distance'])


if __name__ == '__main__':
    h5_file = '/afs/cern.ch/user/t/tdingley/private/neighbourjet/data/gn2/user.tdingley.521296.e8551_s4159_r14799_p5981.tdd.EMPFlow.25_0_2.24-04-08-T154217_output.h5/user.tdingley.38069986._000001.output.h5'
    min_distance_threshold = 2
    flavour = 'b'
    main(h5_file, min_distance_threshold, flavour)
